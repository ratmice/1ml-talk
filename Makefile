1ml_COMPILER=./1ml

#.PHONY : 1.foo 2.foo 3.foo 4.foo 5.foo 6.foo 7.foo 8.foo 9.foo auth.foo order.foo

1 : 1.1ml
2 : 2.1ml
3 : 3.1ml
4 : 4.1ml
5 : 5.1ml | 4.1ml
6 : 6.1ml | 4.1ml
7 : 7.1ml | 4.1ml
8 : 8.1ml | 4.1ml 7.1ml

auth : auth.1ml | 4.1ml 7.1ml
order : order.1ml
discrim : discrim.1ml | prelude.1ml 4.1ml 7.1ml

% : %.1ml
	$(1ml_COMPILER) $| $^
