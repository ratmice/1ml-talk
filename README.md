1: Records, tuples, basic syntax

2: Various kinds of functions, applicative functors

3: Sharing types with "with", structural subtyping, transparent ascription

4: Simple generative functor

5: Large and small types.

6: Slightly more complex functor.

7: Phantom types

8: Employing 7 into 5

auth: using phantom types, large types, pure functors, and currying as authentication.

discrim: Another employment of 8 with option for predicated types.

option: Wrapping small types into large ones, option/Maybe, and case.

order:  Leveraging an already wrapped type. 

https://gitlab.com/ratmice/1ml-talk

The toy 1ml interpreter and papers are available from Andreas Rossberg's site:
https://people.mpi-sws.org/~rossberg/1ml/
